# **Generador de Instalador x32 / x64**

## **Archivos necesarios para generar el instalador**

- Archivos necesarios para instalacion del software

  - NAME_APP.exe
  - NAME_APP.jar
  - Icono.ico
  - ConfigBD
    - InicializarServer.bat
    - CargarBD.txt

- Archivos necesarios de Softwares externos

  - java-8.0.291-win32.exe / jre-8u371-windows-x64.exe
  - mariadb-10.3.10-win32.msi / mariadb-10.3.10-winx64.msi
  - vcredist_x86.exe / vc_redist.x64.exe

## **Pasos a seguir para generar el instalador**

1. Editar de **Script-NAME_APP-NSIS-win32.nsi / Script-NAME_APP-NSIS-winx64.nsi** la linea 7 y 8, con version y nombre de la app a instalar.
2. En ConfigBD > CargarBD editar el nombre de la base de datos y llenarlas con las tablas correspondientes.
3. Generar el .jar del software. Esto se hace desde eclipse haciendo click derecho sobre el proyecto > Export > Java > Runnable JAR file > finish
4. Generar el .exe del software a traves de la aplicacion Launch4j y llenar los siguientes datos:

- Output file - Es donde se va generar el archivo.exe.
- Jar - Hace referencia al archivo generado del paso 1.
- Icon - En el icono del exe que se va a genera.
  Luego en la seccion JRE completar 'Min JRE version' con 1.8 y finalmente hacer click en 'Build wrapper'

5. El ultima paso es hacer click derecho en **Script-NAME_APP-NSIS-win32.nsi / Script-NAME_APP-NSIS-winx64.nsi** Mostrar más opciones > Compile NSIS Script.
