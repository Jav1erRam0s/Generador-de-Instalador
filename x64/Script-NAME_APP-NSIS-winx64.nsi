;----------------------------------------------------------------------------------------------------------------
# General
;----------------------------------------------------------------------------------------------------------------

	!include "MUI.nsh"  
	!define AUTHOR "Jav1erRam0s"
	!define VERSION "X.X"
	!define NOMBREAPP "NAME_APP"
	!define SO "winx64"
	
	!define MUI_ICON "Icono.ico"
	!define MUI_HEADERIMAGE 
	!define MUI_HEADERIMAGE_BITMAP "Icono.ico"
	!define MUI_HEADERIMAGE_RIGHT

	Name "${NOMBREAPP}" ;Establecer el nombre de la aplicacion
	Icon "Icono.ico"
	OutFile "${NOMBREAPP}-v${VERSION}-setup-${SO}.exe"       ;Establecer el nombre del archivo de instalacion
   	SetCompressor LZMA          ;El LZMA como algoritmo de compresion

	;Directorio de instalacion
	DirText "Elija un directorio donde instalar la aplicacion:"
	InstallDir "$PROGRAMFILES\${NOMBREAPP}" # InstallDir "c:\${NOMBREAPP}"	

	;Si se encuentran archivos existentes se sobreescriben
	SetOverwrite on
	SetDatablockOptimize on

;----------------------------------------------------------------------------------------------------------------
# Paginas
;----------------------------------------------------------------------------------------------------------------

	;paginas referentes al instalador
	!insertmacro MUI_PAGE_COMPONENTS
	!insertmacro MUI_PAGE_DIRECTORY ;Pagina para elegir el directorio de instalacion
	!insertmacro MUI_PAGE_INSTFILES ;Pagina para de instalacion de los archivos

	;paginas referentes al desinstalador
	!insertmacro MUI_UNPAGE_CONFIRM
	!insertmacro MUI_UNPAGE_INSTFILES

;----------------------------------------------------------------------------------------------------------------
# Lenguaje
;----------------------------------------------------------------------------------------------------------------

	;Definimos el idioma del instalador 
	!insertmacro MUI_LANGUAGE "Spanish"  ;Define el lenguaje en espanol 

;----------------------------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------------------------
Section "${NOMBREAPP}"
;----------------------------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------------------------

SectionIn RO

	SetOutPath "$INSTDIR"
	File "${NOMBREAPP}.jar"
  	File "Icono.ico"
	File "${NOMBREAPP}.exe"
	File /r "ConfigBD"

	;Menu inicio
	SetShellVarContext all
	createDirectory "$SMPROGRAMS\${NOMBREAPP}"
	createShortCut "$SMPROGRAMS\${NOMBREAPP}\${NOMBREAPP}.lnk" "$INSTDIR\${NOMBREAPP}.jar" "" "$INSTDIR\Icono.ico"
	createShortCut "$SMPROGRAMS\${NOMBREAPP}\Desinstalar.lnk" "$INSTDIR\Uninstall.exe" "" ""

	;Acceso directo en el escritorio
 	CreateShortCut "$DESKTOP\${NOMBREAPP}.lnk" "$INSTDIR\${NOMBREAPP}.jar" "" "$INSTDIR\Icono.ico"
	#CreateShortCut "$DESKTOP\${NOMBREAPP}.lnk" "$INSTDIR\${NOMBREAPP}.exe"

	;Creamos un desintalador
	WriteUninstaller "$INSTDIR\Uninstall.exe"

SectionEnd

;----------------------------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------------------------
Section "Prerequisitos" Prerequisitos
;----------------------------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------------------------

SectionIn RO

	DetailPrint "Comenzando la instalacion de Visual C++"
	File "vc_redist.x64.exe"
	ExecWait "$INSTDIR\vc_redist.x64.exe /s" 

	DetailPrint "Comenzando la instalacion de MySQL"	   
  	Var /GLOBAL pathAppData
  	StrCpy $pathAppData "$APPDATA\${NOMBREAPP}" 
    	File "mariadb-10.3.10-winx64.msi"
    	SetShellVarContext all
	ExecWait '"$SYSDIR\msiExec" /i "mariadb-10.3.10-winx64.msi" INSTALLDIR=$pathAppData INSTALLDIR2=$pathAppData PORT=3306 ALLOWREMOTEROOTACCESS=1 ADDLOCAL=ALL REMOVE=HeidiSQL PASSWORD=root SERVICENAME=MySQL /passive'
	;La siguiente instruccion elimina el HeidiSQL -> ALLOWREMOTEROOTACCESS=1 ADDLOCAL=ALL REMOVE=HeidiSQL 

	DetailPrint "Comenzando ConfigBD - Carga de estructura de BD"
 	SetOutPath $INSTDIR
	ExpandEnvStrings $0 %COMSPEC%
    	nsExec::ExecToStack '"$INSTDIR\ConfigBD\InicializarServer.bat"'

	DetailPrint "Comenzando la instalacion de Java"     
    	File "jre-8u371-windows-x64.exe"
    	ExecWait "$INSTDIR\jre-8u371-windows-x64.exe /s" 

SectionEnd  

;----------------------------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------------------------
Section "Uninstall"
;----------------------------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------------------------

	SetShellVarContext all

	;Borramos el ejecutable del menu inicio
	delete "$SMPROGRAMS\${NOMBREAPP}\${NOMBREAPP}.lnk"
	delete "$SMPROGRAMS\${NOMBREAPP}\Desinstalar.lnk"

	;Borramos el acceso directo del escritorio
	delete "$DESKTOP\${NOMBREAPP}.lnk"

	;Intentamos borrar el menu inicio (Solo se puede hacer si la carpeta esta vacio)
	rmDir "$SMPROGRAMS\${NOMBREAPP}"
 
	;Archivos a desinstalar
	delete $INSTDIR\${NOMBREAPP}.jar
	delete $INSTDIR\Icono.ico
	delete $INSTDIR\${NOMBREAPP}.exe

	delete $INSTDIR\ConfigBD\InicializarServer.bat
	delete $INSTDIR\ConfigBD\CargarBD.txt
	rmDir $INSTDIR\ConfigBD

	;Borramos el desinstalador
	delete $INSTDIR\Uninstall.exe

	;Desinstalo MySQL
	ExecWait '"$SYSDIR\msiExec" /x "$INSTDIR\mariadb-10.3.10-winx64.msi" /qb /quiet'

	;Borramos MySQL
	delete $INSTDIR\mariadb-10.3.10-winx64.msi

	;Desinstalo Java
	ExecWait '"$INSTDIR\jre-8u371-windows-x64.exe" /qb /s'

	;Borramos Java
	delete $INSTDIR\jre-8u371-windows-x64.exe

	;Desinstalo framework Visual C++
	ExecWait '"$INSTDIR\vc_redist.x64.exe" /qb'

	;Borramos framework Visual C++
	delete $INSTDIR\vc_redist.x64.exe

	;Intentamos borrar la carpeta de instalacion (Solo se puede si esta vacia)
	rmDir $INSTDIR

	DeleteRegKey /ifempty HKCU "${NOMBREAPP}"

SectionEnd

;----------------------------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------------------------


#Descripciones

	;Descripcion de ${NOMBREAPP}
	LangString DESC_${NOMBREAPP} ${LANG_SPANISH} "Software ${NOMBREAPP}"

	;Descripcion de Prerequisitos
	LangString DESC_Prerequisitos ${LANG_SPANISH} "Otros softwares para que ${NOMBREAPP} funcione correctamente"

	;Asignamos las descripciones a cada seccion
	!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
	!insertmacro MUI_DESCRIPTION_TEXT ${NOMBREAPP} $(DESC_${NOMBREAPP})
	!insertmacro MUI_DESCRIPTION_TEXT ${Prerequisitos} $(DESC_Prerequisitos)
	!insertmacro MUI_FUNCTION_DESCRIPTION_END

;----------------------------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------------------------